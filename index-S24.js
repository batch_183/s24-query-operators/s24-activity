/*
Activity

Instructions s24 Activity:
1. Create an activity.js file on where to write 
and save the solution for the activity.
2. Find users with the letter “s” in their first name or d in their last name.
 2.1 Use the $or operator.
 2.2Show only the firstName and lastName fields and hide the _id field.
3. Find users who are from the HR department and their age is greater than or equal to 70.
 3.1 Use the $and operator
4. Find users with the letter e in their first name and have an age of less than or equal to 30.
 4.1Use the $and, $regex and $lte operators
5. Create a git repository named S24.
6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
7. Add the link in Boodle.
*/


//Activity #2
db.users.find({$or:[{"firstName":{$regex: "s", $options: "$i"}},{"firstName":{$regex: "d", $options: "$i"}}]})

db.users.find(
	{"firstName": "Jane"},
	{
		"firstName": 1,
		"lastName": 1,
		"_id": 0
	}
);


//Activity #3

db.users.find({$and: [{"department": "HR"}, {"age": {$gte: 70}}]});

//Activity $4

db.users.find({$and:[{"firstName":{$regex: "e", $options: "$i"}},{"age": {$lte: 30}}]})



